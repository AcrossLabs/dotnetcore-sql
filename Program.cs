﻿using Microsoft.AspNetCore.Hosting;
using Kontena.Examples.Models;
namespace dotnet_example
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
              .UseKestrel()
              .UseStartup<Startup>()
              .Build();
            host.Run();
        }
    }
}